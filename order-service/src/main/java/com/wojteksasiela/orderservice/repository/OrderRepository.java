package com.wojteksasiela.orderservice.repository;

import com.wojteksasiela.orderservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
