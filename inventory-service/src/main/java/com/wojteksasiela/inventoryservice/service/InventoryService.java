package com.wojteksasiela.inventoryservice.service;

import com.wojteksasiela.inventoryservice.dto.InventoryResponse;
import com.wojteksasiela.inventoryservice.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class InventoryService {

    private final InventoryRepository inventoryRepository;

    @Transactional(readOnly = true)
    public List<InventoryResponse> isInStock(List<String> skuCode) {

        return inventoryRepository.findBySkuCodeIn(skuCode) // findBySkuCodeIn
                .stream() // every
                .map(inventory -> //element of inventory
                    InventoryResponse.builder() // create in InventoryResponse
                            .skuCode(inventory.getSkuCode()) //skuCode
                            .isInStock(inventory.getQuantity()>0) //true or false
                            .build()
                ).toList();

    }
}
