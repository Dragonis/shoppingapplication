* ShoppingApplication
    * Inventory-service
      - Database: Load products to inventory 
      - Api: Check sku-code is in inventory  
    * Order-service
      - API: Create in database entries of product orders
    * Product-service
      - API: Create product
      - API: ShowAllProducts

Project describe:
Client (order-service) makes products order from shop (inventory-service) which products deliveries from provider (product-service)